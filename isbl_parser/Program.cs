﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace isbl_parser
{
    using Antlr4.Runtime;
    using Antlr4.Runtime.Misc;
    using Antlr4.Runtime.Tree;
    using grammar;
    using System.IO;

    class ISBLAstPrinter: ISBLParserBaseVisitor<string>
    {
        private int indent;

        public override string VisitCompilationUnit([NotNull] ISBLParser.CompilationUnitContext context)
        {
            indent = 1;
            var result = this.Visit(context.statements());
            return result;
        }

        public override string VisitStatements([NotNull] ISBLParser.StatementsContext context)
        {
            //indent++;
            try
            {
                return context._stmnts
                    .Select(stmnt => 
                    {
                        indent++;
                        try
                        {
                            return this.Visit(stmnt);
                        }
                        finally
                        {
                            indent--;
                        }
                    })
                    .SelectMany(stmnt => stmnt.Split(new string[] { Environment.NewLine }, StringSplitOptions.None))
                    .Select(s => '<' + indent.ToString() + ':' + ((indent - 1) * 2).ToString() + '>' + new String(' ', (indent - 1) * 2) + s)
                    .Aggregate((acc, s) => acc + Environment.NewLine + s);
            }
            finally
            {
                //indent--;
            }
        }

        public override string VisitStatement([NotNull] ISBLParser.StatementContext context)
        {
            var result = base.VisitStatement(context);
            result = result == null ? "" : result;
            return result;
        }

        public override string VisitAssignStatement([NotNull] ISBLParser.AssignStatementContext context)
        {
            var args = context.args != null ? context.LBRACKET().GetText() + this.Visit(context.args) + context.RBRACKET().GetText() : "";
            var variable = this.Visit(context.variable);
            var value = this.Visit(context.value);

            var result = String.Format(@"{0}{1} = {2}", variable, args, value);
            return result;
        }

        public override string VisitQualifiedIdentifier([NotNull] ISBLParser.QualifiedIdentifierContext context)
        {
            var result = context.GetText();
            return result;
        }

        public override string VisitNumericConst([NotNull] ISBLParser.NumericConstContext context)
        {
            var result = context.constValue.Text;
            return result;
        }

        public override string VisitStringConst([NotNull] ISBLParser.StringConstContext context)
        {
            return context.constValue.Text;
        }

        public override string VisitBooleanConst([NotNull] ISBLParser.BooleanConstContext context)
        {
            return context.constValue.Text;
        }

        public override string VisitNullConst([NotNull] ISBLParser.NullConstContext context)
        {
            return context.constValue.Text;
        }

        public override string VisitParenOpExpression([NotNull] ISBLParser.ParenOpExpressionContext context)
        {
            var args = context.args != null ? this.Visit(context.args) : "";
            var name = this.Visit(context.name);

            return String.Format("{0}({1})", name, args);
        }

        public override string VisitArgExprList([NotNull] ISBLParser.ArgExprListContext context)
        {
            return context._args
                .Select(ec => ec != null ? this.Visit(ec) : "")
                .Aggregate((acc, s) => acc + ';' + s);
        }

        public override string VisitBracketOpExpression([NotNull] ISBLParser.BracketOpExpressionContext context)
        {
            var args = context.args != null ? this.Visit(context.args) : "";
            var name = this.Visit(context.name);

            return String.Format("{0}[{1}]", name, args);
        }

        public override string VisitBinaryExpression([NotNull] ISBLParser.BinaryExpressionContext context)
        {
            var left = this.Visit(context.left);
            var right = this.Visit(context.right);

            return String.Format("{0} {1} {2}", left, context.op.Text, right);
        }

        public override string VisitCallStatement([NotNull] ISBLParser.CallStatementContext context)
        {
            var args = context.args != null ? this.Visit(context.args) : "";
            var name = this.Visit(context.name);

            return String.Format("{0}({1})", name, args);
        }

        public override string VisitParenExpression([NotNull] ISBLParser.ParenExpressionContext context)
        {
            return String.Format("({0})", this.Visit(context.expression()));
        }

        public override string VisitUnaryExpression([NotNull] ISBLParser.UnaryExpressionContext context)
        {
            var value = this.Visit(context.value);

            return String.Format("{0} {1}", context.op.Text, value);
        }

        public override string VisitVariableExpresion([NotNull] ISBLParser.VariableExpresionContext context)
        {
            return this.Visit(context.name);
        }

        public override string VisitWhileStatement([NotNull] ISBLParser.WhileStatementContext context)
        {
            var condition = this.Visit(context.condition);
            var statements = "";
            indent--;
            try
            {
                statements = this.Visit(context.statements());
            }
            finally
            {
                indent++;
            }

            return String.Format("while {0}\r\n{1}\r\nendwhile", condition, statements);
        }

        public override string VisitIfThenElseStatement([NotNull] ISBLParser.IfThenElseStatementContext context)
        {
            var condition = this.Visit(context.condition);
            var thenStmnt = "";
            var elseStmnt = "";
            indent--;
            try
            {
                thenStmnt = this.Visit(context.thenStmnt);
                elseStmnt = context.elseStmnt != null ? this.Visit(context.elseStmnt) : "";
            }
            finally
            {
                indent++;
            }

            var sb = new StringBuilder();

            sb
                .Append("if ").AppendLine(condition)
                .AppendLine(thenStmnt);

            if (elseStmnt != "")
                sb
                    .AppendLine("elseif")
                    .Append(elseStmnt);
            sb
                .Append("endif");

            return sb.ToString();
        }

        public override string VisitNilConst([NotNull] ISBLParser.NilConstContext context)
        {
            return context.constValue.Text;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var file = File.OpenRead(@"F:\Programming\antlrv4\isbl_parser\examples\Reports.scr");

            var stream = new AntlrInputStream(file);
            ITokenSource lexer = new ISBLLexer(stream);
            ITokenStream tokens = new CommonTokenStream(lexer);

            var parser = new ISBLParser(tokens);
            parser.BuildParseTree = true;
            IParseTree tree = parser.compilationUnit();

            var mirBuilder = new mirAstBuilder();
            Console.WriteLine(mirBuilder.Visit(tree));

            //var printer = new ISBLAstPrinter();
            //Console.WriteLine(printer.Visit(tree));

            //var nodeFactory = new NodeFactory();
            //var parserVisitor = new ISBLParserVisitor<NodeFactory, Node>(nodeFactory);
            //var node = parserVisitor.Visit(tree);
        }
    }
}
