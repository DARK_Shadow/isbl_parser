﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isbl_parser
{
    class NodeFactory : INodeFactory<Node>
    {
        public Node CreateArgList<Arg>(IEnumerable<Arg> args) where Arg : Node
        {
            return new ArgsListNode((dynamic)args);
        }

        public Node CreateAssignStatement<LVal, LArgs, RVal>(LVal variable, LArgs args, RVal value)
            where LVal : Node
            where LArgs : Node
            where RVal : Node
        {
            return new AssignStmntNode((dynamic)variable, (dynamic)args, (dynamic)value);
        }

        public Node CreateBinaryExpression<Expr, Op>(Expr left, Op op, Expr right)
            where Expr : Node
            where Op : Node
        {
            return new BinaryExpressionNode((dynamic)left, (dynamic)right, (dynamic)op);
        }

        public Node CreateBooleanConst(bool value)
        {
            return new ConstNode<bool>(value);
        }

        public Node CreateBracketOpExpression<Name, Args>(Name name, Args args)
            where Name : Node
            where Args : Node
        {
            return new BracketExprNode((dynamic)name, (dynamic)args);
        }

        public Node CreateCallStatement<Name, Args>(Name name, Args args)
            where Name : Node
            where Args : Node
        {
            return new CallStmntNode((dynamic)name, (dynamic)args);
        }

        public Node CreateIfThenElseStatement<Expr, Stmnt>(Expr condition, Stmnt thenStatements, Stmnt elseStatements)
            where Expr : Node
            where Stmnt : Node
        {
            throw new NotImplementedException();
        }

        public Node CreateNilConst()
        {
            return new ConstNode<string>("nil");
        }

        public Node CreateNullConst()
        {
            return new ConstNode<string>("null");
        }

        public Node CreateNullNode()
        {
            return new NullNode();
        }

        public Node CreateNumericConst(string value)
        {
            return new ConstNode<string>(value);
        }

        public Node CreateOperator(string op)
        {
            return new OpNode(op);
        }

        public Node CreateParenOpExpression<Name, Args>(Name name, Args args)
            where Name : Node
            where Args : Node
        {
            return new ParenExprNode((dynamic)name, (dynamic)args);
        }

        public Node CreateQualifiedIdentifier(string qIdentifier)
        {
            return new IdentifierNode(qIdentifier);
        }

        public Node CreateStatements<Stmnt>(IEnumerable<Stmnt> statement) where Stmnt : Node
        {
            return new StmntsNode((dynamic)statement);
        }

        public Node CreateStringConst(string value)
        {
            return new ConstNode<string>(value);
        }

        public Node CreateUnaryExpression<Expr, Op>(Op op, Expr value)
            where Expr : Node
            where Op : Node
        {
            throw new NotImplementedException();
        }

        public Node CreateUnit<Stmnts>(Stmnts statements) where Stmnts : Node
        {
            return new UnitNode((dynamic)statements);
        }

        public Node CreateVariable<Name>(Name name) where Name : Node
        {
            return new VariableNode((dynamic)name);
        }

        public Node CreateWhileStatement<Expr, Stmnts>(Expr condition, Stmnts statements)
            where Expr : Node
            where Stmnts : Node
        {
            throw new NotImplementedException();
        }
    }
}
