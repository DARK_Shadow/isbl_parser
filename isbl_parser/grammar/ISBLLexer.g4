/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
lexer grammar ISBLLexer;

IF : 'if' | 'If' ;
ELSE : 'else' | 'Else' ;
ENDIF : 'endif' | 'EndIf' ;
OR : 'or' | 'Or' ;
AND : 'and' | 'And' ;
NOT : 'not' | 'Not' ;
FOREACH : 'foreach' | 'ForEach' ;
ENDFOREACH : 'endforeach' | 'EndEorEach' ;
WHILE : 'while' | 'While' ;
ENDWHILE : 'endwhile' | 'EndWhile' ;
IN : 'in' | 'In' ;
EXITFOR : 'exitfor' | 'ExitFor' ;
TRY : 'try' | 'Try' ;
EXCEPT : 'except' | 'Except' ;
FINALLY : 'finally' | 'Finally' ;
ENDEXCEPT : 'endexcept' | 'EndExcept' ;
ENDFINALLY : 'endfinally' | 'EndFinally' ;
NULL : 'null' | 'NULL' ;
NIL : 'nil' | 'NIL' ;
TRUE : 'true' | 'TRUE';
FALSE : 'false' | 'FALSE';
NUMERIC 
    : '-'? DIGIT+ '.' DIGIT+ 
    | '-'? DIGIT+
    ;
fragment 
DIGIT 
    : [0-9] 
    ;
STRING 
    : '"' ~["]* '"'
    | '\'' ~[']* '\''
    ;
IDENTIFIER 
    : '!' [a-zA-Z0-9_]+ 
    | [a-zA-Z_] [a-zA-Z0-9_]* 
    ;
ASSIGN : EQ ;
MINUS : '-' ;
PLUS : '+' ;
MUL : '*' ;
DIV : '/' ;
LPAREN : '(' ;
RPAREN : ')' ;
ENDOPERAND 
    : ',' 
    | ';'
    ;
//ENDTEXT : EOF;
NUMBER_EQ : EQ ;
fragment 
    EQ : '=' ;
NUMBER_NEQ : '<>' ;
NUMBER_GTEQ : '>=' ;
NUMBER_LTEQ : '<=' ;
NUMBER_GT : '>' ;
NUMBER_LT : '<' ;
STRING_EQ : '==' ;
STRING_NEQ : '<<>>' ;
STRING_GTEQ : '>>=' ;
STRING_LTEQ : '<<=' ;
STRING_GT : '>>' ;
STRING_LT : '<<' ;
CONCAT : '&' ;
QUALIFIER : '.' ;
//FUNCTION : '' ;
//GO : '' ;
LBRACKET : '[' ;
RBRACKET : ']' ;
OUTTOFILE : '#' ;
NEWLINE : [\r\n]+ -> channel(HIDDEN) ;
//CONST : '' ;
COLON : ':' ;
//DATATYPE : '' ;

BLOCK_COMMENT : '/*' .*? '*/' -> channel(HIDDEN) ;
LINE_COMMENT : '//' ~[\r\n]* -> channel(HIDDEN) ;
WS : [ \r\n\t]+ -> channel(HIDDEN) ;
