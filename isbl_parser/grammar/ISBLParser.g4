/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
parser grammar ISBLParser;

options { tokenVocab=ISBLLexer; }

compilationUnit
    : statements? EOF
    ;

statements 
    : stmnts+=statement+

//    : statement
//    | statement statements
    ;

statement
    : assignStatement
    | callStatement
    | whileStatement
    | ifThenElseStatement
    ;

ifThenElseStatement
    : IF condition=expression thenStmnt=statements (ELSE elseStmnt=statements)? ENDIF
    ;

whileStatement
    : WHILE condition=expression statements ENDWHILE
    ;

callStatement
    : name=qualifiedIdentifier (LPAREN args=argExprList? RPAREN)?
    ;

assignStatement
    : variable=qualifiedIdentifier (LBRACKET args=argExprList RBRACKET)? ASSIGN value=expression
    ;

qualifiedIdentifier
    : IDENTIFIER (QUALIFIER IDENTIFIER)*
    ;

expression
    : op=NOT value=expression #UnaryExpression
    | left=expression op=(NUMBER_EQ | NUMBER_NEQ | STRING_EQ | STRING_NEQ) right=expression #BinaryExpression
    | left=expression op=(NUMBER_LT | NUMBER_GT | STRING_LT | STRING_GT | NUMBER_LTEQ | NUMBER_GTEQ | STRING_LTEQ | STRING_GTEQ) right=expression #BinaryExpression
//    | left=expression op=(STRING_EQ | NUMBER_EQ | STRING_NEQ | STRING_LTEQ | STRING_LT | NUMBER_NEQ | NUMBER_LTEQ | NUMBER_LT | STRING_GTEQ | STRING_LT | NUMBER_GTEQ | NUMBER_GT) right=expression #BinaryExpression
    | left=expression op=(MUL | DIV) right=expression #BinaryExpression
    | left=expression op=(PLUS | MINUS | CONCAT) right=expression #BinaryExpression
    | left=expression op=(OR | AND) right=expression #BinaryExpression
    | name=qualifiedIdentifier LBRACKET args=argExprList RBRACKET #BracketOpExpression
    | name=qualifiedIdentifier LPAREN args=argExprList? RPAREN #ParenOpExpression
    | LPAREN expression RPAREN #ParenExpression
    | name=qualifiedIdentifier #VariableExpresion
    | constValue=(TRUE | FALSE) #BooleanConst
    | constValue=NULL #NullConst
    | constValue=NIL #NilConst
    | constValue=NUMERIC #NumericConst
    | constValue=STRING #StringConst
    ;

argExprList
    : args+=expression (ENDOPERAND args+=expression?)*
    ;