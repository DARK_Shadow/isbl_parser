﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isbl_parser
{
    abstract class Node
    { }

    abstract class ExprNode: Node
    { }

    abstract class StmntNode: Node
    { }

    //abstract class OpNode: Node
    //{ }

    abstract class ErrorNode: Node
    { }

    class NullNode: Node
    { }

    class BinaryExpressionNode: ExprNode
    {
        public ExprNode Left { get; private set; }
        public ExprNode Right { get; private set; }
        public OpNode Op { get; private set; }

        public BinaryExpressionNode(ExprNode Left, ExprNode Right, OpNode Op)
        {
            this.Left = Left;
            this.Right = Right;
            this.Op = Op;
        }
    }

    class OpNode: Node
    {
        public string Op { get; private set; }

        public OpNode(string op)
        {
            this.Op = op;
        }
    }

    //class AppOpNode: OpNode
    //{ }

    //class SubOpNode: OpNode
    //{ }

    //class DivOpNode: OpNode
    //{ }

    //class MulOpNode: OpNode
    //{ }

    //class EqOpNode: OpNode
    //{ }

    //class NEqOpNode: OpNode
    //{ }

    //class GTOpNode: OpNode
    //{ }

    //class GTEqOpNode: OpNode
    //{ }

    //class LTOpNode: OpNode
    //{ }

    //class LTEqOpNode: OpNode
    //{ }

    abstract class ValueNode: ExprNode
    { }

    abstract class ConstNode: ValueNode
    { }

    class ConstNode<T>: ConstNode
    {
        public T Value { get; private set; }

        public ConstNode(T value)
        {
            this.Value = value;
        }
    }

    class IdentifierNode: Node
    {
        public string Value { get; private set; }

        public IdentifierNode(string value)
        {
            this.Value = value;
        }
    }

    class ArgsListNode: Node
    {
        public IEnumerable<ExprNode> Args { get; private set; }

        public ArgsListNode(IEnumerable<ExprNode> args)
        {
            Args = args;
        }
    }

    class VariableNode: ValueNode
    {
        public IdentifierNode Ident { get; private set; }

        public VariableNode(IdentifierNode ident)
        {
            Ident = ident;
        }
    }

    class AssignStmntNode: StmntNode
    {
        public VariableNode LValue { get; private set; }
        public ArgsListNode LArgs { get; private set; }
        public ExprNode RValue { get; private set; }

        public AssignStmntNode(VariableNode lvalue, ArgsListNode largs, ExprNode rvalue)
        {
            LValue = lvalue;
            LArgs = largs;
            RValue = rvalue;
        }
    }

    abstract class CallExprNode: ExprNode
    {
        public IdentifierNode Name { get; private set; }
        public ArgsListNode Args { get; private set; }

        public CallExprNode(IdentifierNode name, ArgsListNode args)
        {
            Name = name;
            Args = args;
        }
    }

    class ParenExprNode: CallExprNode
    {
        public ParenExprNode(IdentifierNode name, ArgsListNode args) : base(name, args)
        { }
    }

    class BracketExprNode: CallExprNode
    {
        public BracketExprNode(IdentifierNode name, ArgsListNode args) : base(name, args)
        { }
    }

    class CallStmntNode: StmntNode
    {
        public IdentifierNode Name { get; private set; }
        public ArgsListNode Args { get; private set; }

        public CallStmntNode(IdentifierNode name, ArgsListNode args)
        {
            Name = name;
            Args = args;
        }
    }

    class StmntsNode: StmntNode
    {
        public IEnumerable<StmntNode> Stmnts { get; private set; }

        public StmntsNode(IEnumerable<StmntNode> stmnts)
        {
            Stmnts = stmnts;
        }
    }

    class UnitNode: Node
    {
        public StmntsNode Statements { get; private set; }
        public UnitNode(StmntsNode stmnts)
        {
            Statements = stmnts;
        }
    }
}
