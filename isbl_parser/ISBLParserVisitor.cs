﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isbl_parser
{
    using Antlr4.Runtime.Misc;
    using Antlr4.Runtime.Tree;
    using grammar;

    interface INodeFactory<T>
    {
        T CreateNullNode();
        T CreateArgList<Arg>(IEnumerable<Arg> args) where Arg: T;
        T CreateQualifiedIdentifier(string qIdentifier);
        T CreateAssignStatement<LVal, LArgs, RVal>(LVal variable, LArgs args, RVal value) where LVal: T where LArgs: T where RVal: T;
        T CreateOperator(string op);
        T CreateBinaryExpression<Expr, Op>(Expr left, Op op, Expr right) where Expr: T where Op: T;
        T CreateBooleanConst(bool value);
        T CreateBracketOpExpression<Name, Args>(Name name, Args args) where Name: T where Args: T;
        T CreateCallStatement<Name, Args>(Name name, Args args) where Name: T where Args: T;
        T CreateUnit<Stmnts>(Stmnts statements) where Stmnts: T;
        T CreateIfThenElseStatement<Expr, Stmnt>(Expr condition, Stmnt thenStatements, Stmnt elseStatements) where Expr: T where Stmnt: T;
        T CreateNullConst();
        T CreateNilConst();
        T CreateNumericConst(string value);
        T CreateParenOpExpression<Name, Args>(Name name, Args args) where Name: T where Args: T;
        T CreateStatements<Stmnt>(IEnumerable<Stmnt> statement) where Stmnt: T;
        T CreateStringConst(string value);
        T CreateUnaryExpression<Expr,Op>(Op op, Expr value) where Expr: T where Op: T;
        T CreateVariable<Name>(Name name) where Name: T;
        T CreateWhileStatement<Expr, Stmnts>(Expr condition, Stmnts statements) where Expr: T where Stmnts: T;
    }

    class ISBLParserVisitor<F, T>: ISBLParserBaseVisitor<T> where F: INodeFactory<T>
    {
        private readonly F factory;

        public ISBLParserVisitor(F factory) : base()
        {
            this.factory = factory;
        }

        private T VisitOrDefault(IParseTree tree, Func<T> defFunc)
        {
            return tree != null ? Visit(tree) : defFunc();
        }

        public override T VisitArgExprList([NotNull] ISBLParser.ArgExprListContext context)
        {
            return factory.CreateArgList(context._args
                .Select(ec => VisitOrDefault(ec, factory.CreateNullNode)));
        }

        public override T VisitAssignStatement([NotNull] ISBLParser.AssignStatementContext context)
        {
            var rvalue = Visit(context.value);
            var largs = VisitOrDefault(context.args, factory.CreateNullNode);
            var lvalue = Visit(context.variable);

            return factory.CreateAssignStatement(lvalue, largs, rvalue);
        }

        public override T VisitBinaryExpression([NotNull] ISBLParser.BinaryExpressionContext context)
        {
            var left = Visit(context.left);
            var right = Visit(context.right);
            var op = factory.CreateOperator(context.op.Text);

            return factory.CreateBinaryExpression(left, op, right);
        }

        public override T VisitBooleanConst([NotNull] ISBLParser.BooleanConstContext context)
        {
            return factory.CreateBooleanConst(Boolean.Parse(context.constValue.Text));
        }

        public override T VisitBracketOpExpression([NotNull] ISBLParser.BracketOpExpressionContext context)
        {
            var args = VisitOrDefault(context.args, factory.CreateNullNode);
            var name = Visit(context.name);

            return factory.CreateBracketOpExpression(name, args);
        }

        public override T VisitCallStatement([NotNull] ISBLParser.CallStatementContext context)
        {
            var args = VisitOrDefault(context.args, factory.CreateNullNode);
            var name = Visit(context.name);

            return factory.CreateCallStatement(name, args);
        }

        public override T VisitCompilationUnit([NotNull] ISBLParser.CompilationUnitContext context)
        {
            return factory.CreateUnit(
                VisitOrDefault(context.statements(), factory.CreateNullNode)); 
        }

        public override T VisitErrorNode(IErrorNode node)
        {
            return base.VisitErrorNode(node);
        }

        public override T VisitExpression([NotNull] ISBLParser.ExpressionContext context)
        {
            return base.VisitExpression(context);
        }

        public override T VisitIfThenElseStatement([NotNull] ISBLParser.IfThenElseStatementContext context)
        {
            var condition = Visit(context.condition);
            var thenStmnts = VisitOrDefault(context.thenStmnt, factory.CreateNullNode);
            var elseStmnts = VisitOrDefault(context.elseStmnt, factory.CreateNullNode);

            return factory.CreateIfThenElseStatement(condition, thenStmnts, elseStmnts);
        }

        public override T VisitNilConst([NotNull] ISBLParser.NilConstContext context)
        {
            return factory.CreateNilConst();
        }

        public override T VisitNullConst([NotNull] ISBLParser.NullConstContext context)
        {
            return factory.CreateNullConst();
        }

        public override T VisitNumericConst([NotNull] ISBLParser.NumericConstContext context)
        {
            return factory.CreateNumericConst(context.constValue.Text);
        }

        public override T VisitParenExpression([NotNull] ISBLParser.ParenExpressionContext context)
        {
            return Visit(context.expression());
        }

        public override T VisitParenOpExpression([NotNull] ISBLParser.ParenOpExpressionContext context)
        {
            var args = VisitOrDefault(context.args, factory.CreateNullNode);
            var name = Visit(context.name);

            return factory.CreateParenOpExpression(name, args);
        }

        public override T VisitQualifiedIdentifier([NotNull] ISBLParser.QualifiedIdentifierContext context)
        {
            return factory.CreateQualifiedIdentifier(context.GetText());
        }

        public override T VisitStatement([NotNull] ISBLParser.StatementContext context)
        {
            return base.VisitStatement(context);
        }

        public override T VisitStatements([NotNull] ISBLParser.StatementsContext context)
        {
            var stmnts = context._stmnts
                .Select(stmnt => Visit(stmnt));
            return factory.CreateStatements(stmnts);
        }

        public override T VisitStringConst([NotNull] ISBLParser.StringConstContext context)
        {
            return factory.CreateStringConst(context.constValue.Text);
        }

        public override T VisitUnaryExpression([NotNull] ISBLParser.UnaryExpressionContext context)
        {
            var value = Visit(context.value);
            var op = factory.CreateOperator(context.op.Text);

            return factory.CreateUnaryExpression(op, value);
        }

        public override T VisitVariableExpresion([NotNull] ISBLParser.VariableExpresionContext context)
        {
            return factory.CreateVariable(
                Visit(context.name));
        }

        public override T VisitWhileStatement([NotNull] ISBLParser.WhileStatementContext context)
        {
            return factory.CreateWhileStatement(
                Visit(context.condition),
                VisitOrDefault(context.statements(), factory.CreateNullNode));
        }
    }
}
