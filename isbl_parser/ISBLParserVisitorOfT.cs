﻿using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using isbl_parser.grammar;
using System;
using System.Collections.Generic;
using System.Linq;

namespace isbl_parser
{
    abstract class ISBLParserVisitor<T> : ISBLParserBaseVisitor<T>
    {
        protected abstract T CreateNullNode();
        protected abstract T CreateArgList(IEnumerable<T> args);
        protected abstract T CreateAssignStatement(T variable, T args, T value);
        protected abstract T CreateOperator(string op);
        protected abstract T CreateBinaryExpression(T left, T op, T right);
        protected abstract T CreateBooleanConst(string value);
        protected abstract T CreateBracketOpExpression(T name, T args);
        protected abstract T CreateCallStatement(T name, T args);
        protected abstract T CreateUnit(T statements);
        protected abstract T CreateIfThenElseStatement(T condition, T thenStatements, T elseStatements);
        protected abstract T CreateNullConst(string value);
        protected abstract T CreateParenOpExpression(T name, T args);
        protected abstract T CreateIdentifier(string value);
        protected abstract T CreateQualifiedIdentifier(IEnumerable<T> value);
        protected abstract T CreateStatements(IEnumerable<T> statements);
        protected abstract T CreateStringConst(string value);
        protected abstract T CreateUnaryExpression(T op, T value);
        protected abstract T CreateVariable(T name);
        protected abstract T CreateWhileStatement(T condition, T statements);

        private T VisitOrDefault(IParseTree tree, Func<T> defFunc)
        {
            return tree != null ? Visit(tree) : defFunc();
        }

        public override T VisitArgExprList([NotNull] ISBLParser.ArgExprListContext context)
        {
            var args = context._args
                .Select(ec => VisitOrDefault(ec, CreateNullNode));
            return CreateArgList(args);
        }

        public override T VisitAssignStatement([NotNull] ISBLParser.AssignStatementContext context)
        {
            var rvalue = Visit(context.value);
            var largs = VisitOrDefault(context.args, CreateNullNode);
            var lvalue = Visit(context.variable);

            return CreateAssignStatement(lvalue, largs, rvalue);
        }

        public override T VisitBinaryExpression([NotNull] ISBLParser.BinaryExpressionContext context)
        {
            var left = Visit(context.left);
            var right = Visit(context.right);
            var op = CreateOperator(context.op.Text);

            return CreateBinaryExpression(left, op, right);
        }

        public override T VisitBooleanConst([NotNull] ISBLParser.BooleanConstContext context)
        {
            return CreateBooleanConst(context.constValue.Text);
        }

        public override T VisitBracketOpExpression([NotNull] ISBLParser.BracketOpExpressionContext context)
        {
            var args = VisitOrDefault(context.args, CreateNullNode);
            var name = Visit(context.name);

            return CreateBracketOpExpression(name, args);
        }

        public override T VisitCallStatement([NotNull] ISBLParser.CallStatementContext context)
        {
            var args = VisitOrDefault(context.args, CreateNullNode);
            var name = Visit(context.name);

            return CreateCallStatement(name, args);
        }

        public override T VisitCompilationUnit([NotNull] ISBLParser.CompilationUnitContext context)
        {
            var stmnts = VisitOrDefault(context.statements(), CreateNullNode);
            return CreateUnit(stmnts);
        }

        public override T VisitErrorNode(IErrorNode node)
        {
            return base.VisitErrorNode(node);
        }

        public override T VisitExpression([NotNull] ISBLParser.ExpressionContext context)
        {
            return base.VisitExpression(context);
        }

        public override T VisitIfThenElseStatement([NotNull] ISBLParser.IfThenElseStatementContext context)
        {
            var condition = Visit(context.condition);
            var thenStmnts = VisitOrDefault(context.thenStmnt, CreateNullNode);
            var elseStmnts = VisitOrDefault(context.elseStmnt, CreateNullNode);

            return CreateIfThenElseStatement(condition, thenStmnts, elseStmnts);
        }

        protected abstract T CreateNilConst(string value);

        public override T VisitNilConst([NotNull] ISBLParser.NilConstContext context)
        {
            return CreateNilConst(context.GetText());
        }

        public override T VisitNullConst([NotNull] ISBLParser.NullConstContext context)
        {
            return CreateNullConst(context.GetText());
        }

        protected abstract T CreateNumericConst(string value);

        public override T VisitNumericConst([NotNull] ISBLParser.NumericConstContext context)
        {
            return CreateNumericConst(context.constValue.Text);
        }

        public override T VisitParenExpression([NotNull] ISBLParser.ParenExpressionContext context)
        {
            return Visit(context.expression());
        }

        public override T VisitParenOpExpression([NotNull] ISBLParser.ParenOpExpressionContext context)
        {
            var args = VisitOrDefault(context.args, CreateNullNode);
            var name = Visit(context.name);

            return CreateParenOpExpression(name, args);
        }

        public override T VisitQualifiedIdentifier([NotNull] ISBLParser.QualifiedIdentifierContext context)
        {
            var qIdent = context.IDENTIFIER()
                .Select(i => CreateIdentifier(i.GetText()));
            return CreateQualifiedIdentifier(qIdent);
        }

        public override T VisitStatement([NotNull] ISBLParser.StatementContext context)
        {
            return base.VisitStatement(context);
        }

        public override T VisitStatements([NotNull] ISBLParser.StatementsContext context)
        {
            var stmnts = context._stmnts
                .Select(stmnt => Visit(stmnt));
            return CreateStatements(stmnts);
        }

        public override T VisitStringConst([NotNull] ISBLParser.StringConstContext context)
        {
            return CreateStringConst(context.constValue.Text);
        }

        public override T VisitUnaryExpression([NotNull] ISBLParser.UnaryExpressionContext context)
        {
            var value = Visit(context.value);
            var op = CreateOperator(context.op.Text);

            return CreateUnaryExpression(op, value);
        }

        public override T VisitVariableExpresion([NotNull] ISBLParser.VariableExpresionContext context)
        {
            var name = Visit(context.name);
            return CreateVariable(name);
        }

        public override T VisitWhileStatement([NotNull] ISBLParser.WhileStatementContext context)
        {
            var condition = Visit(context.condition);
            var statements = VisitOrDefault(context.statements(), CreateNullNode);

            return CreateWhileStatement(condition, statements);
        }
    }
}
