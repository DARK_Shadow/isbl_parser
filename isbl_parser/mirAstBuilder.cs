﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace isbl_parser
{
    class mirAstBuilder : ISBLParserVisitor<string>
    {
        Dictionary<string, int> symbolMap = new Dictionary<string, int>();
        List<string> symbols = new List<string>();

        private int AddSymbol(string value)
        {
            var index = -1;
            if (!symbolMap.TryGetValue(value, out index))
            {
                index = symbols.Count;
                symbols.Add(value);
                symbolMap.Add(value, index);
            }
            return index;
        }

        protected override string CreateArgList(IEnumerable<string> args)
        {
            return @"L(" + args.Aggregate((a, s) => a + @"," + s) + @")";
        }

        protected override string CreateAssignStatement(string variable, string args, string value)
        {
            return string.Format(@"SA({0},{1},{2})", variable, args, value);
        }

        protected override string CreateBinaryExpression(string left, string op, string right)
        {
            return string.Format(@"EB({0},{1},{2})", left, op, right);
        }

        protected override string CreateBooleanConst(string value)
        {
            return string.Format(@"CB{0}", value);
        }

        protected override string CreateBracketOpExpression(string name, string args)
        {
            return string.Format(@"EBO({0},{1})", name, args);
        }

        protected override string CreateCallStatement(string name, string args)
        {
            return string.Format(@"SC({0},{1})", name, args);
        }

        protected override string CreateIdentifier(string value)
        {
            return string.Format(@"I{0}", AddSymbol(value));
        }

        protected override string CreateIfThenElseStatement(string condition, string thenStatements, string elseStatements)
        {
            return string.Format(@"SI({0},{1},{2})", condition, thenStatements, elseStatements);
        }

        protected override string CreateNilConst(string value)
        {
            return string.Format(@"CI{0}", AddSymbol(value));
        }

        protected override string CreateNullConst(string value)
        {
            return string.Format(@"CU{0}", AddSymbol(value));
        }

        protected override string CreateNullNode()
        {
            return @"NN";
        }

        protected override string CreateNumericConst(string value)
        {
            return string.Format(@"CN{0}", AddSymbol(value));
        }

        protected override string CreateOperator(string op)
        {
            return string.Format(@"O{0}", AddSymbol(op));
        }

        protected override string CreateParenOpExpression(string name, string args)
        {
            return string.Format(@"EPO({0})", name, args);
        }

        protected override string CreateQualifiedIdentifier(IEnumerable<string> value)
        {
            return string.Format(@"Q({0})", string.Join(@",", value));
        }

        protected override string CreateStatements(IEnumerable<string> statements)
        {
            //return string.Format(@"SS({0})", string.Join(@"," + Environment.NewLine, statements));
            return string.Format(@"SS({0})", string.Join(@",", statements));
        }

        protected override string CreateStringConst(string value)
        {
            return string.Format(@"CS{0}", AddSymbol(value));
        }

        protected override string CreateUnaryExpression(string op, string value)
        {
            return string.Format(@"EU({0},{1})", op, value);
        }

        protected override string CreateUnit(string statements)
        {
            return string.Format(@"U({0})", statements) + Environment.NewLine + string.Join(Environment.NewLine, symbolMap.Select(p => string.Format("[{1}]{0}", p.Key, p.Value)));
        }

        protected override string CreateVariable(string name)
        {
            return string.Format(@"EV({0})", name);
        }

        protected override string CreateWhileStatement(string condition, string statements)
        {
            return string.Format(@"SW({0},{1})", condition, statements);
        }
    }
}
